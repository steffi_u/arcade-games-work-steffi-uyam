# lab Camel
# Mr. Birrell

import random


# Define game for restart

def mouse_game():

    # Story

    print("\nWelcome to \"Cat Escape\"")
    print("\nYou are a bright purple mouse that fell off the counter on to a sleeping cat!")
    print("Luckily you ran 2 m away before it woke up and your favourite hole in the wall is only 65 m away.")
    print("Make sure to check your status often! Good luck, and have fun!")

    # Set variables

    meters_travelled = 0
    hunger = 0
    tiredness = 0
    distance_cat = -2
    bites_of_cheese = 2

    # Give options

    done = False
    while not done:
        print("\n-----------------------------------------------------------------------------------------------------")
        print("A. Run as fast as you can")
        print("B. Run at a regular speed")
        print("E. Eat cheese")
        print("R. Rest")
        print("S. Status summary")
        print("Q. Quit")
        print("I. Instructions")
        print("-----------------------------------------------------------------------------------------------------")

        user_input = input("\nWhat will you do?: ")

        # Option A

        if user_input.upper() == "A" or user_input.upper() == "RUN AS FAST AS YOU CAN":
            meters_per_turn = random.randrange(5, 8)
            meters_travelled += meters_per_turn
            hunger += 2
            tiredness += random.randrange(1, 3)
            distance_cat += random.randrange(3, 6)
            print("\nYou can feel the wind blow by your face." + " You travelled " + str(meters_per_turn) + " meters.")

        # Random event for hiding spot

            hiding_spot = random.randrange(1, 15)
            if hiding_spot == 5:
                tiredness *= 0
                print("\n\tYou are running so fast you can barely see, so you trip and fall into a nice warm shoe.")
                print("\tThe cat has no idea where you are but is determined to find you.")
                print("\nYour tiredness is fully restored.")

        # Option B

        elif user_input.upper() == "B" or user_input.upper() == "RUN AT A REGULAR SPEED":
            meters_per_turn = random.randrange(3, 6)
            meters_travelled += meters_per_turn
            hunger += 2
            tiredness += 1
            distance_cat += random.randrange(4, 7)
            print("\nYou scurry across the floor." + " You travelled " + str(meters_travelled) + " meters.")

            # Mouse trap random event

            mouse_trap = random.randrange(1, 11)
            if mouse_trap == 10:
                print("\n\tAs you are running you see a delicious piece of cheese.")
                take_cheese = input("\n\tWill you take it? ")
                if take_cheese.upper() == "TAKE" or take_cheese.upper() == "YES" or take_cheese.upper() == "OK":
                    outcome = random.randrange(0, 2)
                    if outcome == 1:
                        print("\nYou reach for the cheese, but, you are too slow and it closes down on you!")
                        print("There is no way to escape the cat now!")
                        print("\n\t\t GAME OVER")
                        done = True
                        retry = input("\n\tTry again?: ")
                        if retry.upper() == "YES" or retry.upper() == "OK":
                            mouse_game()
                        else:
                            done = True
                    elif outcome == 0:
                        print("\nYou quickly snatch the cheese out of the trap.")
                        print("\nYour hunger and cheese refill.")
                        hunger *= 0
                        bites_of_cheese += 2
                    elif take_cheese.upper() == "NO":
                        print("You decided it looks way too dangerous to take and keep running.")

            # Cat trip random event

            cat_falls = random.randrange(1, 14)
            if cat_falls == 10:
                print("\n\tWhile you are running you hear a loud CRASH! When you look back you see the cat has")
                print("fallen and gotten its head stuck in a mop bucket.")
                print("\nYou gain some distance on the cat.")
                distance_cat -= 6

        # Option E

        elif user_input.upper() == "E" or user_input.upper() == "EAT CHEESE":
            distance_cat += random.randrange(2, 4)
            if bites_of_cheese >= 1:
                print("\nYou pull out a small piece of cheese and take a bite.")
                hunger *= 0
                bites_of_cheese -= 1
            elif bites_of_cheese <= 0:
                print("\nError! Too bad, you don't have any cheese!")

        # Option R

        elif user_input.upper() == "R" or user_input.upper() == "REST":
            print("\nYou stop to catch your breath and you are filled with energy.")
            hunger += 2
            tiredness = 0
            distance_cat += random.randrange(2, 4)

        # Option Q

        elif user_input.upper() == "Q" or user_input.upper() == "QUIT":
            done = True

        # Option I

        elif user_input.upper() == "I" or user_input.upper() == "INSTRUCTIONS":
            print("\nEvery time you run, drink, or rest the cat will will be running towards you. ")
            print("If your hunger or tiredness reaches 100% you will lose.")
            print("If the cat catches up you will also lose.")
            print("Have fun!")
            print("If you check your summary or the instructions, dont worry! You will not lose a turn.")

        # Variable for calculation of the distance between mouse and cat

        distance_from_cat = meters_travelled - distance_cat

        # Option S

        if user_input.upper() == "S" or user_input.upper() == "STATUS SUMMARY":
            print("\nYou have travelled " + str(meters_travelled) + " meters.")
            print("You have " + str(int(bites_of_cheese)) + " cheese left.")
            print("You are " + str(int(hunger) * 10) + "% hungry.")
            print("You are " + str(int(tiredness) * 10) + "% tired.")
            print("The cat is " + str(distance_from_cat) + " meters behind you.")

        # Warnings and Endings

            # Output if win

        if meters_travelled >= 65:
            print("\n\tCongratulations you made it all the way to your hole!")
            done = True
            retry = input("\n\tPlay again?: ")
            if retry.upper() == "YES" or retry.upper() == "OK":
                mouse_game()
            else:
                done = True

            # Output if cat is close or cat catches up

        if not done and distance_from_cat <= 3 and distance_from_cat > 0:
            print("Pick up the pace! The cat is getting close!")
        elif not done and distance_from_cat <= 0:
            print("\nOH NO! The cat caught up!")
            print("You made it " + str(meters_travelled) + " meters.")
            print("\n\t\tGAME OVER")
            done = True
            retry = input("\n\tTry again?: ")
            if retry.upper() == "YES" or retry.upper() == "OK":
                mouse_game()
            else:
                done = True

            # Output for if hungry or die of hunger

        if not done and hunger >= 6 and hunger < 10:
            print("Your stomach is growling.")
        elif not done and hunger >= 10:
            print("\nBad choice, you died of hunger.")
            print("You made it " + str(meters_travelled) + " meters.")
            print("\n\t\tGAME OVER")
            done = True
            retry = input("\n\tTry again?: ")
            if retry.upper() == "YES" or retry.upper() == "OK":
                mouse_game()
            else:
                done = True

            # Output if tired or too tired to keep going

        if not done and tiredness >= 6 and tiredness < 10:
            print("You are getting really tired.")
        elif not done and tiredness >= 10:
            print("\nBad choice, you are way to tired to keep running.")
            print("You made it " + str(meters_travelled) + " meters.")
            print("\n\t\tGAME OVER")
            done = True
            retry = input("\n\tTry again?: ")
            if retry.upper() == "YES" or retry.upper() == "OK":
                mouse_game()
            else:
                done = True


# Run game

mouse_game()
