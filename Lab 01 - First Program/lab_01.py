# import arcade

print("February 6, 2018")
print("Steffi Uyam")
print("Lab 01\n")
print("Once upon a time, an ogre lived under a bridge.")
print("He was lonely and sad.")
print("\nBut one day, a boy was skipping down the edge of the bridge.")
print("The boy fell over the bridge but the ogre caught him.")
print("\"Thank you,\" the boy said.")
print("The ogre and the boy became friends.")
print("They lived happily ever after.")
print("The end.")
