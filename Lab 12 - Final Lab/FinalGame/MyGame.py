"""
 Pygame base template for opening a window

 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/

 Explanation video: http://youtu.be/vRB_983kUMc
"""
import random
import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
PURPLE = (163, 73, 164)


class Player(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()
        # Load the image looking to the right
        self.image_right = pygame.image.load("bunny.png").convert()
        self.image_right.set_colorkey(WHITE)

        # Load the image again, flipping it, so it points left
        self.image_left = pygame.transform.flip(pygame.image.load("bunny.png").convert(), True, False)
        self.image_left.set_colorkey(WHITE)

        # By default, point right
        self.image = self.image_right

        self.rect = self.image.get_rect()

        # Sets the Starting Position
    def move(self, change_x, change_y):
        """ Change the speed of the player. Called with a keypress. """
        self.rect.x += change_x
        self.rect.y += change_y

        # Select if we want the left or right image based on the direction
        # we are moving.
        if change_x > 0:
            self.image = self.image_left

        elif change_x < 0:
            self.image = self.image_right


class Log(pygame.sprite.Sprite):

    def __init__(self, x):
        super().__init__()
        self.image = pygame.image.load("log.png").convert()
        self.image.set_colorkey(WHITE)
        self.rect = self.image.get_rect()
        self.speed_x = x

    def update(self):

        self.rect.x += self.speed_x


class Carrot(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()

        self.image = pygame.image.load("carrot.png").convert()

        # Set background color to be transparent. Adjust to WHITE if your
        # background is WHITE.
        self.image.set_colorkey(WHITE)

        self.rect = self.image.get_rect()


frame_count = 0
frame_rate = 60
start_time = 20

pygame.init()

# Set the width and height of the screen [width, height]
SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720
size = (SCREEN_WIDTH, SCREEN_HEIGHT)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Bunny Crossing")

# Loop until the user clicks the close button
done = False

score = 0


# Used to manage how fast the screen updates
clock = pygame.time.Clock()

font = pygame.font.Font(None, 25)

pygame.mouse.set_visible(False)

background_image = pygame.image.load("river.jpg").convert()

all_sprites_list = pygame.sprite.Group()
log_list = pygame.sprite.Group()
carrot_list = pygame.sprite.Group()
player_list = pygame.sprite.Group()


for i in range(50):
    # This represents a block
    carrot = Carrot()

    # Set a random location for the block
    carrot.rect.x = random.randrange(SCREEN_WIDTH - 20)
    carrot.rect.y = random.randrange(SCREEN_HEIGHT)

    # Add the block to the list of objects
    carrot_list.add(carrot)
    all_sprites_list.add(carrot)


for i in range(5):
    log = Log(1)

    log.rect.x = random.randrange(-500, SCREEN_WIDTH - 500)
    log.rect.y = 80
    log_list.add(log)
    all_sprites_list.add(log)
for i in range(3):
    log = Log(-1)

    log.rect.x = random.randrange(1280 - 500, SCREEN_WIDTH + 500)
    log.rect.y = 225
    log_list.add(log)
    all_sprites_list.add(log)
for i in range(4):
    log = Log(1)

    log.rect.x = random.randrange(-500, SCREEN_WIDTH - 500)
    log.rect.y = 375
    log_list.add(log)
    all_sprites_list.add(log)
for i in range(3):
    log = Log(-1)

    log.rect.x = random.randrange(1280 - 500, SCREEN_WIDTH + 500)
    log.rect.y = 525
    log_list.add(log)
    all_sprites_list.add(log)

player = Player()
player.rect.x = (SCREEN_WIDTH/2)
player.rect.y = (SCREEN_HEIGHT-100)
player_list.add(player)
all_sprites_list.add(player)

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:

            if event.key == pygame.K_a:
                player.move(-50, -20)

            if event.key == pygame.K_d:
                player.move(50, -20)

            if event.key == pygame.K_w:
                player.move(0, -150)

            if event.key == pygame.K_s:
                player.move(0, -10)

        if event.type == pygame.KEYUP:

            if event.key == pygame.K_a:
                player.move(-50, 20)

            if event.key == pygame.K_d:
                player.move(50, 20)

            if event.key == pygame.K_w:
                player.move(0, 10)

            if event.key == pygame.K_s:
                player.move(0, 150)

        if player.rect.x > SCREEN_WIDTH:
            done = True

        if player.rect.x < SCREEN_WIDTH - 1280:
            done = True

    # --- Game logic should go here

    # Calculate total seconds
    total_seconds = frame_count // frame_rate

    # Divide by 60 to get total minutes
    minutes = total_seconds // 60

    # Use modulus (remainder) to get seconds
    seconds = total_seconds % 60

    # Use python string formatting to format in leading zeros
    output_string = "Time: {0:02}:{1:02}".format(minutes, seconds)

    # Blit to the screen

    # --- Timer going down ---
    # --- Timer going up ---
    # Calculate total seconds
    total_seconds = start_time - (frame_count // frame_rate)
    if total_seconds < 0:
        total_seconds = 0
        done = True

    # Divide by 60 to get total minutes
    minutes = total_seconds // 60

    # Use modulus (remainder) to get seconds
    seconds = total_seconds % 60

    # Use python string formatting to format in leading zeros
    output_string = "Time left: {0:02}:{1:02}".format(minutes, seconds)

    for log in log_list:
        if log.rect.x == SCREEN_WIDTH + 50 and log.rect.y == 80 or log.rect.x == SCREEN_WIDTH + 50 \
                and log.rect.y == 375:
            log.rect.x = random.randrange(-1500, -200)

        if log.rect.x == -245 and log.rect.y == 225 or log.rect.x == -245 and log.rect.y == 525:
            log.rect.x = random.randrange(1290, 2000)

        player_hit_list = pygame.sprite.spritecollide(log, player_list, False)

        for i in player_hit_list:
            player.rect.x += log.speed_x
            on_log = True

    carrots_hit_list = pygame.sprite.spritecollide(player, carrot_list, True)

    for carrots in carrots_hit_list:
        score += 1
        print(score)

    print(score)
    print(output_string)

    all_sprites_list.update()

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.

    # --- Drawing code should go here

    screen.blit(background_image, [0, 0])
    score_font = pygame.font.SysFont('Comic Sans MS', 25, True, False)
    score_text = score_font.render("Score: " + str(score), True, WHITE)

    all_sprites_list.draw(screen)
    screen.blit(score_text, [50, 30])

    output_string = "Time: {0:02}:{1:02}".format(minutes, seconds)

    timer_text = font.render(output_string, True, WHITE)

    screen.blit(timer_text, [620, 0])

    frame_count += 1

    # --- Limit to 60 frames per second
    clock.tick(frame_rate)

    # updates the screen with what is drawn
    pygame.display.flip()

# Close the window and quit.
pygame.quit()
