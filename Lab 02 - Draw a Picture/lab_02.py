"""
Lab 2
"""

# Import the "arcade" library
import arcade

# Open up a window
# From the "arcade" library, use a function called "open_window"
# Set the window title to "Ring Toss"
# Set the dimensions (width and height)
arcade.open_window(1280, 720, "Ring Toss")

# Set the background color
arcade.set_background_color((0, 255, 0))

# Get ready to draw
arcade.start_render()

# Drawing code will go here

# Draw the rectangular base of toy, grey-ish
arcade.draw_rectangle_filled(640, 360, 610, 356, (200, 191, 231))
arcade.draw_rectangle_outline(640, 360, 610, 354, arcade.color.BLACK, 1)

# Draw the screen of toy, light blue
arcade.draw_rectangle_filled(640, 360, 610, 264, (128, 255, 255))
arcade.draw_rectangle_outline(640, 360, 610, 262, arcade.color.BLACK, 1)

# Draw the text "RING TOSS MACHINE" on the base (font "Berlin Sans FB Demi"), black
arcade.draw_text("RING TOSS  MACHINE", 443, 192, arcade.color.BLACK, 30, 500, font_name='Berlin Sans FB Demi')

# Draw rectangular strips on left and right side of base, black
arcade.draw_rectangle_filled(309, 360, 52, 354, arcade.color.BLACK)
arcade.draw_rectangle_filled(960, 360, 52, 354, arcade.color.BLACK)

# Draw left arc base, red
arcade.draw_arc_filled(283, 360, 176, 177, (237, 28, 36), 90, 270)
arcade.draw_arc_outline(283, 360, 176, 177, arcade.color.BLACK, 90, 270)

# Draw right arc base, blue
arcade.draw_arc_filled(986, 360, 176, 177, (0, 162, 232), 90, 270, 180)
arcade.draw_arc_outline(986, 360, 176, 177, arcade.color.BLACK, 90, 270, 1, 180)

# Draw on/off button, grey-ish
arcade.draw_ellipse_filled(1040, 226, 35, 17, (200, 191, 231))

# Draw "On/Off" text on the button (font "Calibri"), black
arcade.draw_text("On/Off", 1012, 220, arcade.color.BLACK, 14, 52)

# Draw outline of the buttons on the right, red
arcade.draw_circle_filled(1090, 372, 59, (237, 28, 36))
arcade.draw_circle_filled(1063, 304, 37, (237, 28, 36))

# Draw the slider button on the right, blue
arcade.draw_circle_filled(1063, 304, 25, (0, 162, 232))
arcade.draw_circle_outline(1063, 304, 26, arcade.color.BLACK, 2.5)

# Draw arrow buttons on the right, lime green
arcade.draw_rectangle_filled(1091, 378, 24, 84, (128, 255, 0))
arcade.draw_rectangle_filled(1091, 378, 24, 84, (128, 255, 0), 90)

# Draw lines to represent the arrows on the arrow button, black
arcade.draw_line(1092, 344, 1092, 366, arcade.color.BLACK, 6)
arcade.draw_line(1104, 378, 1126, 378, arcade.color.BLACK, 6)
arcade.draw_line(1092, 394, 1092, 413, arcade.color.BLACK, 6)
arcade.draw_line(1056, 378, 1079, 378, arcade.color.BLACK, 6)
arcade.draw_rectangle_outline(1092, 378, 6, 6, arcade.color.BLACK, 8)

# Draw outline of the buttons on the left, blue
arcade.draw_circle_filled(180, 372, 59, (0, 162, 232))
arcade.draw_circle_filled(220, 304, 37, (0, 162, 232))

# Draw slider button on the right, red
arcade.draw_circle_filled(220, 304, 25, (237, 28, 36))
arcade.draw_circle_outline(220, 304, 26, arcade.color.BLACK, 2.5)


# Draw four small buttons on the left, lime green
arcade.draw_circle_filled(181, 342, 18, (128, 255, 0))
arcade.draw_circle_filled(212, 373, 18, (128, 255, 0))
arcade.draw_circle_filled(181, 400, 18, (128, 255, 0))
arcade.draw_circle_filled(148, 373, 18, (128,  255, 0))

# Draw a square, a circle, a triangle, and an X on the four buttons, black
arcade.draw_rectangle_outline(213, 373, 16, 16, arcade.color.BLACK, 5)
arcade.draw_circle_outline(148, 373, 10, arcade.color.BLACK, 5)
arcade.draw_triangle_outline(180, 414, 170, 393, 192, 393, arcade.color.BLACK, 5)
arcade.draw_line(172, 352, 190, 331, arcade.color.BLACK, 5)
arcade.draw_line(172, 331, 190, 352, arcade.color.BLACK, 5)

# Draw the speaker, grey-ish
arcade.draw_point(165, 292, (200, 191, 231), 2)
arcade.draw_point(155, 292, (200, 191, 231), 2)
arcade.draw_point(145, 292, (200, 191, 231), 2)
arcade.draw_point(145, 282, (200, 191, 231), 2)
arcade.draw_point(155, 282, (200, 191, 231), 2)
arcade.draw_point(165, 282, (200, 191, 231), 2)

# Draw the camera, grey-ish
arcade.draw_circle_outline(242, 475, 7, (200, 191, 231))
arcade.draw_circle_outline(242, 475, 13, (200, 191, 231))

# Draw the 'start' and 'select' button, grey-ish
arcade.draw_ellipse_filled(210, 226, 17, 8, (200, 191, 231))
arcade.draw_ellipse_filled(254, 226, 17, 8, (200, 191, 231))

# Draw text 'Start', black
arcade.draw_text("Start", 196, 221, arcade.color.BLACK, 10, 4)

# Draw text 'Select', black
arcade.draw_text("Select", 238, 221, arcade.color.BLACK, 10, 4)

# Draw three swords on the screen, grey-ish brown
arcade.draw_line(640, 360, 640, 251, (185, 122, 87), 10)
arcade.draw_line(640, 290, 660, 270, (185, 122, 87), 10)
arcade.draw_line(640, 290, 620, 270, (185, 122, 87), 10)
arcade.draw_line(475, 454, 475, 345, (185, 122, 87), 10)
arcade.draw_line(475, 384, 495, 364, (185, 122, 87), 10)
arcade.draw_line(475, 384, 455, 364, (185, 122, 87), 10)
arcade.draw_line(805, 454, 805, 345, (185, 122, 87), 10)
arcade.draw_line(805, 384, 825, 364, (185, 122, 87), 10)
arcade.draw_line(805, 384, 785, 364, (185, 122, 87), 10)

# Draw hoops on screen, green
arcade.draw_circle_outline(572, 272, 14, arcade.color.GREEN, 3)
arcade.draw_circle_outline(911, 435, 14, arcade.color.GREEN, 3)
arcade.draw_circle_outline(841, 449, 14, arcade.color.GREEN, 3)
arcade.draw_circle_outline(786, 440, 14, arcade.color.GREEN, 3)
arcade.draw_circle_outline(521, 311, 14, arcade.color.GREEN, 3)
arcade.draw_circle_outline(533, 427, 14, arcade.color.GREEN, 3)

# Draw hoops on screen, red
arcade.draw_circle_outline(565, 315, 14, arcade.color.RED, 3)
arcade.draw_circle_outline(521, 292, 14, arcade.color.RED, 3)
arcade.draw_circle_outline(572, 272, 14, arcade.color.RED, 3)
arcade.draw_circle_outline(652, 392, 14, arcade.color.RED, 3)
arcade.draw_circle_outline(810, 454, 14, arcade.color.RED, 3)
arcade.draw_circle_outline(866, 376, 14, arcade.color.RED, 3)

# Draw hoops on screen, blue
arcade.draw_circle_outline(478, 441, 14, (63, 72, 204), 3)
arcade.draw_circle_outline(748, 375, 14, (63, 72, 204), 3)
arcade.draw_circle_outline(865, 342, 14, (63, 72, 204), 3)
arcade.draw_circle_outline(878, 389, 14, (63, 72, 204), 3)

# Draw hoops on screen, yellow
arcade.draw_circle_outline(522, 431, 14, arcade.color.YELLOW, 3)
arcade.draw_circle_outline(552, 411, 14, arcade.color.YELLOW, 3)
arcade.draw_circle_outline(819, 419, 14, arcade.color.YELLOW, 3)

# ---finish drawing---
arcade.finish_render()

# keep the window running until someone closes it
arcade.run()
