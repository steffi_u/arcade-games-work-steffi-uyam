import arcade


SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600


def draw_sand():
    """ Draw the sand"""
    arcade.draw_lrtb_rectangle_filled(0, SCREEN_WIDTH, SCREEN_HEIGHT / 6, 0, (228, 210, 126))


def draw_sky():
    """ Draw the sky """
    arcade.draw_lrtb_rectangle_filled(0, SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_HEIGHT / 1.5, (153, 217, 234))


def draw_cloud(x, y):
    """ Draw clouds """

    # Draw a point at x, y for reference
    arcade.draw_point(x, y, arcade.color.RED, 1)

    arcade.draw_ellipse_filled(x, y, 35, 95, (220, 242, 248), 90)
    arcade.draw_ellipse_filled(x, y, 30, 75, (220, 242, 248), 60)
    arcade.draw_ellipse_filled(x, y, 28, 85, (220, 242, 248), 120)
    arcade.draw_ellipse_filled(x, y, 30, 55, (220, 242, 248), 360)


def draw_waves(x, y):
    """ Draw the water waves """

    # Draw a point at x, y for reference
    arcade.draw_point(x, y, arcade.color.RED, 0.001)

    # Draw waves
    arcade.draw_arc_filled(x, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 51, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 103, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 155, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 207, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 259, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 311, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 363, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 415, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 467, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 519, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 571, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 623, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 675, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 727, y, 25, 18, (153, 217, 234), 0, 180, 180)
    arcade.draw_arc_filled(x + 779, y, 25, 18, (153, 217, 234), 0, 180, 180)


def draw_fish(x, y):
    """ Draw a grey fish """

    # Draw a point at x, y for reference
    arcade.draw_point(x, y, arcade.color.RED, 0.1)

    # Draw head and body
    arcade.draw_arc_filled(x, y, 60, 60, (241, 80, 88), 0, 180, 320)
    arcade.draw_arc_filled(x, y, 60, 60, (241, 80, 88), 0, 180, 220)
    arcade.draw_circle_filled(x - 10, 36 + y, 9, arcade.color.BLACK)

    # Draw tail and fins
    arcade.draw_arc_filled(x + 75, y, 20, 18, (241, 80, 88), 0, 180, 30)
    arcade.draw_arc_filled(x + 75, y, 20, 18, (241, 80, 88), 0, 180, 140)
    arcade.draw_triangle_filled(x - 15, 58 + y, x + 50, 66 + y, x + 30, 46 + y, (241, 80, 88))
    arcade.draw_triangle_filled(x + 5, -58 + y, x + 35, -69 + y, x + 28, -54 + y, (241, 80, 88))

    # Draw bubbles
    arcade.draw_circle_filled(x - 15, -1 + y, 4, (193, 237, 255))
    arcade.draw_circle_filled(x - 35, 12 + y, 8, (193, 237, 255))
    arcade.draw_circle_filled(x - 42, -14 + y, 7, (193, 237, 255))
    arcade.draw_circle_filled(x - 58, -4 + y, 3, (193, 237, 255))
    arcade.draw_circle_filled(x - 58, 18 + y, 2, (193, 237, 255))


def draw_sail(x, y):
    """ Draw a sail """

    # Draw a point at x, y for reference
    arcade.draw_point(x, y, arcade.color.RED, 1)

    # Draw boat parts
    arcade.draw_rectangle_filled(x, y, 87, 41, (185, 122, 87))
    arcade.draw_triangle_filled(x + 91, 20 + y, x + 43, 20 + y, x + 43, -20 + y, (185, 122, 87))
    arcade.draw_triangle_filled(x - 42, -20 + y, x - 88, 20 + y, x - 43, 20 + y, (185, 122, 87))

    # Draw the sail
    arcade.draw_line(x, 20 + y, x, 102 + y, arcade.color.ORANGE, 4)
    arcade.draw_triangle_filled(x, 32 + y, x, 102 + y, x - 80, 32 + y, arcade.color.YELLOW)


def draw_seaweed():
    """ Draw seaweed """
    arcade.draw_line(618, 15, 614, 191, (39, 203, 88), 5.5)
    arcade.draw_line(632, 33, 631, 220, (34, 177, 76), 5.8)
    arcade.draw_line(648, 24, 646, 195, (34, 177, 76), 5.5)
    arcade.draw_line(669, 26, 664, 237, (39, 203, 88), 5.8)


def draw_rock():
    """ Draw a rock """
    arcade.draw_arc_filled(196, 95, 75, 75, arcade.color.GRAY, 0, 180)
    arcade.draw_arc_outline(196, 95, 75, 75, arcade.color.BLACK, 0, 180, 0.2)
    arcade.draw_arc_filled(228, 95, 64, 90, arcade.color.GRAY, 0, 180)
    arcade.draw_arc_outline(228, 95, 64, 90, arcade.color.BLACK, 0, 126, 0.2)


def on_draw(delta_time):
    """ Draw everything """
    arcade.start_render()

    draw_sky()
    draw_sand()
    draw_cloud(on_draw.cloud1_x, 565)
    draw_cloud(on_draw.cloud2_x, 530)
    draw_waves(on_draw.waves1_x, 400)
    draw_waves(on_draw.waves2_x, 400)
    draw_waves(on_draw.waves3_x, 400)
    draw_sail(on_draw.sail_x, 404)
    draw_rock()
    draw_fish(on_draw.fish_x, 195)
    draw_seaweed()

    # My first function (on_draw.waves1_x)
    # subtract 2 to on_draw.waves1_x to move waves left
    # subtract 2.7 to on_draw.waves2_x to move waves left a little faster
    # subtract 3 to on_draw.waves3_x to move waves left a little more faster

    on_draw.waves1_x -= 2
    on_draw.waves2_x -= 2.7
    on_draw.waves3_x -= 3

    # My second function (on_draw.sail_x)
    # add 1.5 to on_draw.sail_x to move sail right

    on_draw.sail_x += 1.5

    # My third function (on_draw.cloud1_x)
    # add 2.3 to on_draw.cloud1_x to move cloud right
    # add 0.6 to on_draw.cloud2_x to move cloud right slower

    on_draw.cloud1_x += 2.3
    on_draw.cloud2_x += 0.6

    # subtract 2.5 to on_draw.fish_x to move fish left

    on_draw.fish_x -= 2.5


# Create a value that on_draw.cloud1_x will start at.

on_draw.cloud1_x = 90

# Create a value that on_draw.cloud2_x will start at

on_draw.cloud2_x = 540

# Create a value that on_draw.waves1_x will start at

on_draw.waves1_x = 25

# Create a value that on_draw.waves2_x will start at

on_draw.waves2_x = 800

# Create a value that on_draw.waves3_x will start at

on_draw.waves3_x = 1600

# Create a value that on_draw.sail_x will start at

on_draw.sail_x = 95

# Create a value that on_draw.fish_x will start at

on_draw.fish_x = 700


def main():
    arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "Sail in the Sea")
    arcade.set_background_color((0, 162, 232))

    # Call on_draw every 60th of a second.
    arcade.schedule(on_draw, 1/60)
    arcade.run()


# Call the main function to get the program started.
main()
