"""
Steffi Uyam
Computer Science 20
March 13, 2018
Lab 4
"""

# Make a Math quiz
print("Complete the Math quiz.")

print()

# Input part

# Make variable for total correct answers
total_score = 0
total_questions = 0

# First question
# Number answer

question_one = int(input("1. Determine the unknown number in this pattern: 2, 2, 4, 12, 48, ? "))
if question_one == 240:
    print("Your answer is Correct!")
    total_score += 1
    total_questions += 1
else:
    print("Your answer is Incorrect.")
    total_questions += 1
print()

# Second question
# Menu answer (multiple choice)

print("2. In Calvin's town, everyone is either a liar or a truth-teller.")
print('Calvin says, "My sister told me that she was a liar."')
print("Is Calvin a liar or a truth-teller?")
print("a) Calvin is a truth-teller")
print("b) Calvin is a liar")
question_two = input("? ")
if question_two.lower() == "b":
    print("Your answer is correct!")
    total_score += 1
    total_questions += 1
else:
    print("Your answer is incorrect.")
    total_questions += 1
print()

# Third question
# Text answer

question_three = input("3. What do you call an adorable angle? ")
if question_three.lower() == "acute" or question_three.lower() == "acute angle":
    print("Your answer is correct!")
    total_score += 1
    total_questions += 1
else:
    print("Your answer is incorrect.")
    total_questions += 1
print()

# Fourth question
# Number answer

question_four = int(input("4. A farmer counted 198 sheep. After rounding them up, how many sheeps does he have? "))
if question_four == 200:
    print("Your answer is correct!")
    total_score += 1
    total_questions += 1
else:
    print("Your answer is incorrect.")
    total_questions += 1
print()

# Fifth question
# Menu answer (true or false)

question_five = input("5. The sum of two odd numbers is always odd. True or False? ")
if question_five.lower() == "false" or question_five.lower() == "f":
    print("Your answer is correct!")
    total_score += 1
    total_questions += 1
else:
    print("Your answer is incorrect.")
    total_questions += 1
print()

# Sixth question
# Number answer

question_six = int(input("6. There are 7 cookies. You take away 4. How many cookies do you have now? "))
if question_six == 4:
    print("Your answer is correct!")
    total_score += 1
    total_questions += 1
else:
    print("Your answer is incorrect.")
    total_questions += 1
print()

# Calculate part

# Make a variable to represent the total percentage of the user's score.
total_percentage = total_score / total_questions * 100

# Output part

# Print the total of correct answers the user got.
print("Congratulations, you got", total_score, "answers right out of", str(total_questions) + ".")

# Print the user's total score percentage.
print("Your total score is " + str(total_percentage) + " percent.")
