"""
Jazmine Itong and Steffi Uyam
Mr. Birrell
Computer Science 20
17 April 2018
"""

import random


# Define game


def snowed_in_survival():

    # Print the background story

    print("\n\tYou were sleeping in your cozy cabin when a snow storm occurred."
          "\n\tYou wake up to find your cabin completely snowed-in and with no power."
          "\n\tTry to survive 48 hours inside your cabin until help comes."
          "\n\t For instructions, type \"HELP\"\n")
    print("------------------------------------------------------------------------------------------------------"
          "--------------------------------------------")

    # Set room list

    room_list = []

    # Set and append room variables

    room = ["\nYou are in the master bedroom. There is a door to the east.", None, 1, None, None]

    room_list.append(room)

    room = ["\nYou are in the West hallway. There is a door to the north and west. The hallway continues to the east.",
            5, 2, None, 0]

    room_list.append(room)

    room = ["\nYou are in the East hallway. There is a door to the east and south. The hallway continues to the"
            " west.", None, 6, 3, 1]

    room_list.append(room)

    room = ["\nYou are in the living room. You look around and see a fireplace and a stack of logs. There is a door "
            "to the north and west.", 2, None, None, 4]

    room_list.append(room)

    room = ["\nYou are in the kitchen. You have a few supply of canned foods. There is a door to the east.",
            None, 3, None, None]

    room_list.append(room)

    room = ["\nYou are in the basement, where you keep a stack of water bottles for emergencies. There is a door to "
            "the south.", None, None, 1, None]

    room_list.append(room)

    room = ["\nYou are in the game room. You can waste time by doing random quizzes here. There is a room to the west.",
            None, None, None, 2]

    room_list.append(room)

    # Set variables

    current_room = 0
    hunger = 5
    canned_food = 2
    thirst = 5
    water_bottle = 3
    tiredness = 0
    cabin_temperature = 10
    wood = 2
    hours_left = 48

    done = False
    while not done:

        print(room_list[current_room][0])

        print()

        user_input = input("\nWhich way do you want to go?: ")
        print("------------------------------------------------------------------------------------------------------"
              "--------------------------------------------")

        # Run the north option

        if user_input.lower() == "w":
            next_room = (room_list[current_room][1])
            if next_room is None:
                print("\tYou can't go that way.")
            else:
                current_room = next_room
                cabin_temperature -= 0.5
                tiredness += 0.25
                thirst += 0.5
                hours_left -= 0.5

            # Run the east option

        elif user_input.lower() == "d":
            next_room = (room_list[current_room][2])
            if next_room is None:
                print("\tYou can't go that way.")
            else:
                current_room = next_room
                cabin_temperature -= 0.5
                tiredness += 0.25
                thirst += 0.5
                hours_left -= 0.5

        # Run the south option

        elif user_input.lower() == "s":
            next_room = (room_list[current_room][3])
            if next_room is None:
                print("\tYou can't go that way.")
            else:
                current_room = next_room
                cabin_temperature -= 0.5
                tiredness += 0.25
                thirst += 0.5
                hours_left -= 0.5

        # Run the west option

        elif user_input.lower() == "a":
            next_room = (room_list[current_room][4])
            if next_room is None:
                print("\tYou can't go that way.")
            else:
                current_room = next_room
                cabin_temperature -= 0.5
                tiredness += 0.25
                thirst += 0.5
                hours_left -= 0.5

        # Run instructions option

        elif user_input.lower() == "help":
            print("\tInstructions:")
            print("\tTo survive, keep track of your hunger, thirst, fatigue, and cabin temperature.")
            print("\tUse your resources carefully to last 48 hours until help arrives.")
            print("\tNorth = \"w\", South = \"s\", West = \"a\", and East = \"d\"")
            print("\tTo see your status summary, type \"?\".")
            print("\tTo quit, type \"QUIT\".")

        elif user_input.lower() == "?":
            print("Hours left until rescued = " + str(int(hours_left)) + " hours")
            print("Hunger = " + str(int(hunger) * 10) + "%")
            print("Thirst = " + str(int(thirst) * 10) + "%")
            print("Fatigue = " + str(int(tiredness) * 10) + "%")
            print("Cabin temperature = " + str(int(cabin_temperature)) + " degrees Celsius")

        # Run the quit option

        elif user_input.upper() == "QUIT":
            print("Thank you for playing!")
            done = True

        # Run the error message

        else:
            print("\n\tSorry, I didn't recognize that request.")
        print()

        # Output if user is in room 0 (master bedroom)

        if current_room == 0:
            print("Do you want to take a nap? Yes or no?")
            take_a_nap = input(": ")
            if take_a_nap.lower() == "yes":
                print("You are now fully rested.")
                tiredness = 0
                hunger += 1
                thirst += 0.5
                cabin_temperature -= 6
                hours_left -= 3
            else:
                print()

        # Output if user is in room 3 (living room)

        elif current_room == 3:
            print("Do you want to add fuel to the fireplace? Yes or no?")
            add_fuel = input(": ")
            if add_fuel.lower() == "yes":
                if wood > 0:
                    tiredness += 2
                    hunger += 1
                    thirst += 0.5
                    cabin_temperature += 10
                    hours_left -= 2
                    wood -= 1
                    print("You add a piece of wood into the fire, increasing the cabin temperature by 10 degrees "
                          "Celsius.")
                    print("You have " + str(int(wood)) + " pieces of wood left.")
                elif wood < 1:
                    print("You do not have any pieces of wood left.")
                print()
            else:
                print("You have " + str(int(wood)) + " pieces of wood left.")
            print()

        # Output if user is in room 4 (kitchen)

        elif current_room == 4:
            print("Do you want to eat? Yes or no?")
            eat_food = input(": ")
            if eat_food.lower() == "yes":
                if canned_food > 0:
                    hunger = 0
                    hours_left -= 1
                    canned_food -= 1
                    print("You open and eat a can of food.")
                    print("You have " + str(int(canned_food)) + " canned food left.")
                elif canned_food < 1:
                    print("You don't have any canned foods left.")
                print()
            else:
                print("You have " + str(int(canned_food)) + " canned food(s) left.")
            print()

        # Output if user is in room 5 (basement)
        # Run a random question every time the user chooses to play a game

        elif current_room == 5:
            print("Do you want to drink water? Yes or no?")
            drink_water = input(": ")
            if drink_water.lower() == "yes":
                if water_bottle > 0:
                    thirst = 0
                    hours_left -= 1
                    water_bottle -= 1
                    print("You are quenched.")
                    print("You have " + str(int(water_bottle)) + " water bottle(s) left.")
                elif water_bottle < 1:
                    print("You don't have any water bottles left.")
                print()
            else:
                print("You have " + str(int(water_bottle)) + " water bottles left.")
            print()

        # Output if user is in room 6 (game room)

        elif current_room == 6:
            print("Do you want to play a game? Yes or no?")
            play_game = input(": ")
            if play_game.lower() == "yes":
                random_question = random.randrange(1, 7)
                if random_question == 1:
                    print("\nWhat is the abbreviation for Washington, D.C.?")
                    print("A - DC")
                    print("B - WD")
                    print("C - WA")
                    question_one = input(": ")
                    if question_one.upper() == "A" or question_one.upper() == "DC":
                        print("Your answer is correct!")
                        hours_left -= 3
                        tiredness += 1
                        hunger += 1
                        cabin_temperature -= 1
                    else:
                        print("Your answer is incorrect.")
                        hours_left -= 2
                        tiredness += 2
                        hunger += 2
                        cabin_temperature -= 2
                    print()
                elif random_question == 2:
                    print("\nAre walruses found in the South Pole?")
                    print("A - Yes")
                    print("B - No")
                    print("C - No they're only found around New Zealand")
                    question_two = input(": ")
                    if question_two.upper() == "B" or question_two.upper() == "NO":
                        print("Your answer is correct!")
                        hours_left -= 3
                        tiredness += 1
                        hunger += 1
                        cabin_temperature -= 1
                        print()
                    else:
                        print("Your answer is incorrect.")
                        hours_left -= 2
                        tiredness += 2
                        hunger += 2
                        cabin_temperature -= 2
                    print()
                elif random_question == 3:
                    print("\nWere jeans always blue?")
                    print("A - Yes")
                    print("B - No")
                    print("C - They were yellow")
                    question_three = input(": ")
                    if question_three.upper() == "B" or question_three.upper() == "NO":
                        print("Your answer is correct!")
                        hours_left -= 3
                        tiredness += 1
                        hunger += 1
                        cabin_temperature -= 1
                        print()
                    else:
                        print("Your answer is incorrect.")
                        hours_left -= 2
                        tiredness += 2
                        hunger += 2
                        cabin_temperature -= 2
                    print()
                elif random_question == 4:
                    print("\nWhere were cats once the most honored?")
                    print("A - Egypt")
                    print("B - Greece")
                    print("C - USA")
                    question_four = input(": ")
                    if question_four.upper() == "A" or question_four.upper() == "EGYPT":
                        print("Your answer is correct!")
                        hours_left -= 3
                        tiredness += 1
                        hunger += 1
                        cabin_temperature -= 1
                        print()
                    else:
                        print("Your answer is incorrect.")
                        hours_left -= 2
                        tiredness += 2
                        hunger += 2
                        cabin_temperature -= 2
                    print()
                elif random_question == 5:
                    print("\nWhat is the largest planet of our Solar System called?")
                    print("A - Saturn")
                    print("B - Earth")
                    print("C - Jupiter")
                    print("D - Uranus")
                    question_five = input(": ")
                    if question_five.upper() == "C" or question_five.upper() == "JUPITER":
                        print("Your answer is correct!")
                        hours_left -= 3
                        tiredness += 1
                        hunger += 1
                        cabin_temperature -= 1
                        print()
                    else:
                        print("Your answer is incorrect.")
                        hours_left -= 2
                        tiredness += 2
                        hunger += 2
                        cabin_temperature -= 2
                    print()
                elif random_question == 6:
                    print("\nWhat is the largest country in the world (by area)?")
                    print("A - United States")
                    print("B - China")
                    print("C - Russia")
                    print("D - Canada")
                    question_six = input(": ")
                    if question_six.upper() == "C" or question_six.upper() == "RUSSIA":
                        print("Your answer is correct!")
                        hours_left -= 3
                        tiredness += 1
                        hunger += 1
                        cabin_temperature -= 1
                        print()
                    else:
                        print("Your answer is incorrect.")
                        hours_left -= 2
                        tiredness += 2
                        hunger += 2
                        cabin_temperature -= 2
                    print()

        # Warnings and Endings

        # Output if the user survives

        if hours_left < 1:
            print("\n\tCongratulations you survived 48 hours of being snowed-in! Rescue finally arrives at your cabin.")
            done = True
            retry = input("\n\tPlay again?: ")
            if retry.upper() == "YES" or retry.upper() == "OK":
                snowed_in_survival()
            else:
                done = True

        # Output if cabin temperature is too low

        if not done and cabin_temperature < 6 and cabin_temperature > -10:
            print("The temperature inside is getting cold!")
        elif not done and cabin_temperature < -9:
            print("\nYou died of hypothermia.")
            print("You lasted " + str(48 - hours_left) + " hours.")
            print("\n\t\tGAME OVER")
            done = True
            retry = input("\n\tTry again?: ")
            if retry.upper() == "YES" or retry.upper() == "OK":
                snowed_in_survival()
            else:
                done = True

        # Output if cabin temperature is too high

        if not done and cabin_temperature > 29  and cabin_temperature < 37:
            print("The temperature inside is getting hot!")
        elif not done and cabin_temperature > 36:
            print("\nYou died of hyperthermia.")
            print("You lasted " + str(48 - hours_left) + " hours.")
            print("\n\t\tGAME OVER")
            done = True
            retry = input("\n\tTry again?: ")
            if retry.upper() == "YES" or retry.upper() == "OK":
                snowed_in_survival()
            else:
                done = True

        # Output for if hungry or die of hunger

        if not done and hunger > 5 and hunger < 10:
            print("Your stomach is growling!")
        elif not done and hunger > 9:
            print("\nYou died of starvation.")
            print("You lasted " + str(48 - hours_left) + " hours.")
            print("\n\t\tGAME OVER")
            done = True
            retry = input("\n\tTry again?: ")
            if retry.upper() == "YES" or retry.upper() == "OK":
                snowed_in_survival()
            else:
                done = True

        # Output for if thirsty or die of thirst

        if not done and thirst > 5 and thirst < 10:
            print("You are really thirsty!")
        elif not done and thirst > 9:
            print("\nYou died of thirst.")
            print("You lasted " + str(48 - hours_left) + " hours.")
            print("\n\t\tGAME OVER")
            done = True
            retry = input("\n\tTry again?: ")
            if retry.upper() == "YES" or retry.upper() == "OK":
                snowed_in_survival()
            else:
                done = True

        # Output if tired or too tired to keep going

        if not done and tiredness > 5 and tiredness < 10:
            print("You are getting really tired!")
        elif not done and tiredness > 9:
            print("\nYou are way too tired and you pass out.")
            print("You lasted " + str(48 - hours_left) + " hours.")
            print("\n\t\tGAME OVER")
            done = True
            retry = input("\n\tTry again?: ")
            if retry.upper() == "YES" or retry.upper() == "OK":
                snowed_in_survival()
            else:
                done = True

# Run game


snowed_in_survival()
