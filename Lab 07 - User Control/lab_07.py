import arcade

# --- Set up the constants

# Size of the screen
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
X = 95
Y = 404

# Size of the rectangle


def draw_sail(X, Y):
    """ Draw a sail """

    # Draw a point at x, y for reference
    arcade.draw_point(X, Y, arcade.color.RED, 1)

    # Draw boat parts
    arcade.draw_rectangle_filled(X, Y, 87, 41, (185, 122, 87))
    arcade.draw_triangle_filled(X + 91, 20 + Y, X + 43, 20 + Y, X + 43, -20 + Y, (185, 122, 87))
    arcade.draw_triangle_filled(X - 42, -20 + Y, X - 88, 20 + Y, X - 43, 20 + Y, (185, 122, 87))

    # Draw the sail
    arcade.draw_line(X, 20 + Y, X, 102 + Y, arcade.color.ORANGE, 4)
    arcade.draw_triangle_filled(X, 32 + Y, X, 102 + Y, X - 80, 32 + Y, arcade.color.YELLOW)


def on_draw(delta_time):
    """
    Use this function to draw everything to the screen.
    """

    # Start the render. This must happen before any drawing
    # commands. We do NOT need a stop render command.
    arcade.start_render()

    draw_sail(on_draw.sail_x, 404)
    draw_sail(95, on_draw.sail_y)

    # Draw a rectangle.
    # For a full list of colors see:
    # http://pythonhosted.org/arcade/arcade.color.html
    # Modify rectangles position based on the delta
    # vector. (Delta means change. You can also think
    # of this as our speed and direction.)
    on_draw.sail_x += on_draw.delta_x * delta_time
    on_draw.sail_y += on_draw.delta_y * delta_time

    # Figure out if we hit the edge and need to reverse.
    if on_draw.sail_x < X // 2 \
            or on_draw.sail_x > SCREEN_WIDTH - X // 2:
        on_draw.delta_x *= -1
    if on_draw.sail_y < Y // 2 \
            or on_draw.sail_y > SCREEN_HEIGHT - Y // 2:
        on_draw.delta_y *= -1

# Below are function-specific variables. Before we use them
# in our function, we need to give them initial values. Then
# the values will persist between function calls.
#
# In other languages, we'd declare the variables as 'static' inside the
# function to get that same functionality.
#
# Later on, we'll use 'classes' to track position and velocity for multiple
# objects.


on_draw.sail_x = 95      # Initial x position
on_draw.sail_y = 404       # Initial y position
on_draw.delta_x = 800  # Initial change in x
on_draw.delta_y = 404  # Initial change in y


def main():
    # Open up our window
    arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "Test")
    arcade.set_background_color(arcade.color.WHITE)

    # Tell the computer to call the draw command at the specified interval.
    arcade.schedule(on_draw, 1 / 60)

    # Run the program
    arcade.run()


if __name__ == "__main__":
    main()
