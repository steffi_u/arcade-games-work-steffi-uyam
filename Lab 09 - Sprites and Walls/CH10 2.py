import pygame

pygame.init()

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)


def draw_stick_figure(screen, x, y):
    # Head
    pygame.draw.ellipse(screen, BLACK, [1 + x, y, 10, 10], 0)

    # Legs
    pygame.draw.line(screen, BLACK, [5 + x, 17 + y], [10 + x, 27 + y], 2)
    pygame.draw.line(screen, BLACK, [5 + x, 17 + y], [x, 27 + y], 2)

    # Body
    pygame.draw.line(screen, WHITE, [5 + x, 17 + y], [5 + x, 7 + y], 2)

    # Arms
    pygame.draw.line(screen, WHITE, [5 + x, 7 + y], [9 + x, 17 + y], 2)
    pygame.draw.line(screen, WHITE, [5 + x, 7 + y], [1 + x, 17 + y], 2)


pygame.init()

size = [700, 500]
screen = pygame.display.set_mode(size)
pos = pygame.mouse.get_pos()

pygame.display.set_caption("My Game")


done = False
clock = pygame.time.Clock()

while done == False:
    clock.tick(10)
    for event in pygame.event.get():
        if event.type == pygame.QUIT():
            done = True

    screen.fill(BLACK)

    draw_stick_figure(screen, 50, 50)
