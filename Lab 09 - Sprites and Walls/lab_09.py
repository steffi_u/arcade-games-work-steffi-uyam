import random

# Part one:


def min3(number_1, number_2, number_3):
    if number_1 < number_2 and number_1 < number_3:
        return number_1

    elif number_2 < number_1 and number_2 < number_3:
        return number_2

    elif number_3 < number_1 and number_3 < number_2:
        return number_3

    elif number_1 == number_2 and number_2 == number_3:
        return number_1 or number_2 or number_3

# Test code


print(min3(4, 7, 5))
print(min3(4, 5, 5))
print(min3(4, 4, 4))
print(min3(-2, -6, -100))
print(min("Z", "B", "A"))

print()

# Part two
# Define box


def box(height, width):
    for i in range(height):
        for a in range(width):
            print("*", end=" ")
        print()

# Test code


box(7, 5)  # Print a box 7 high, 5 across
print()   # Blank line
box(3, 2)  # Print a box 3 high, 2 across
print()   # Blank line
box(3, 10)  # Print a box 3 high, 10 across

print()

# Part three
# Find the position of number in list


def find(a, key):
    for thing in range(len(a)):
        if a[thing] == key:
            print("Found " + str(key) + " at position " + str(thing))

# Test code


my_list = [36, 31, 79, 96, 36, 91, 77, 33, 19, 3, 34, 12, 70, 12, 54, 98, 86, 11, 17, 17]
find(my_list, 12)
find(my_list, 91)
find(my_list, 80)

print()

# Part 4

# First function


def create_list(list_size):
    my_list = []
    for i in range(list_size):
        number = random.randrange(1, 7)
        my_list.append(number)

    return my_list

# Test code


my_list = create_list(5)
print(my_list)

print()

# Second function
# l is list


def count_list(l, number):
    counting_list = 0
    for i in l:
        if i == number:
            counting_list += 1

    return counting_list

# Test code


count = count_list([1, 2, 3, 3, 3, 4, 2, 1], 3)
print(count)

print()

# Third function

# Count the average of the list
# l is list


def average_list(l):
    total_list = 0
    for i in l:
        total_list += i
        avg = total_list / len(l)

    return avg

# Test code


avg = average_list([1, 2, 3])
print(avg)

print()

# Main program
# Numbers from 1-6 10000 times

my_list = create_list(10000)
print()

# Count the numbers of one
count_1 = count_list(my_list, 1)
print("There are", count_1, "number ones.")
print()

# Count the numbers of two
count_2 = count_list(my_list, 2)
print("There are", count_2, "number twos.")
print()

# Count the numbers of three
count_3 = count_list(my_list, 3)
print("There are", count_3, "number threes.")
print()


# Count the numbers of four
count_4 = count_list(my_list, 4)
print("There are", count_4, "number fours.")
print()

# Count the numbers of five
count_5 = count_list(my_list, 5)
print("There are", count_5, "number fives.")
print()


# Count the numbers of six
count_6 = count_list(my_list, 6)
print("There are", count_6, "number sixes.")
print()


# Average of random number

avg = average_list(my_list)
print("The average of this is", str(avg) + ".")
