"""
Steffi Uyam
Computer Science 20
List Exercise
"""

my_list = [7, 4, 2, 7, 3, 4, 6, 7, 8, 9, 7, 0, 10, 7, 0, 1, 7, 6, 5, 7, 3, 2, 7, 9, 9, 8, 7]

# Print my_list

print(my_list)

# Find the sum of spots 3 - 16

total_sum = 0

for i in range(3, 17):
    total_sum += my_list[i]

print("sum of spots 3-16 = " + str(total_sum))


# Find the sum of the spots from 2 - 9

my_list = [7, 4, 2, 7, 3, 4, 6, 7, 8, 9, 7, 0, 10, 7, 0, 1, 7, 6, 5, 7, 3, 2, 7, 9, 9, 8, 7]

total_sum = 0

for i in range(2, 10):
        total_sum += my_list[i]

print("sum of spots 2-9 = " + str(total_sum))


# Displays the number of 0s, 3s, and 7s presents

number_of_zeroes = 0
number_of_threes = 0
number_of_sevens = 0

for thing in my_list:
    if thing == 0:
        number_of_zeroes += 1
    elif thing == 3:
        number_of_threes += 1
    elif thing == 7:
        number_of_sevens += 1

print("# of 0s = " + str(number_of_zeroes))
print("# of 3s = " + str(number_of_threes))
print("# of 7s = " + str(number_of_sevens))


# Creates a new list with all the 7s removed and displays it

new_list = [4, 2, 3, 4, 6, 8, 9, 0, 1, 6, 5, 3, 2, 9, 9, 8]

number_of_sevens_now = 0

for thing in new_list:
    if thing == 7:
        new_list.remove(thing)

print("new list with all 7s removed = " + str(new_list))

for thing in new_list:
    if thing == 7:
        number_of_sevens_now += 1

print("# of 7s =", + number_of_sevens_now)

# Separate the odd and even numbers into their own lists and display the odds and evens.

my_list1 = [2, 4, 6, 8, 10, 12, 14]
my_list2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
my_list3 = [2, 10, 20, 21, 23, 24, 40, 55, 60, 61]

odds1 = []
evens1 = []
odds2 = []
evens2 = []
odds3 = []
evens3 = []

for thing in my_list1:
    if thing % 2 == 0:
        evens1.append(thing)
    else:
        odds1.append(thing)

print("\nOdds = " + str(odds1))
print("Evens = " + str(evens1))

for thing in my_list2:
    if thing % 2 == 0:
        evens2.append(thing)
    else:
        odds2.append(thing)

print("\nOdds = " + str(odds2))
print("Evens = " + str(evens2))

for thing in my_list3:
    if thing % 2 == 0:
        evens3.append(thing)
    else:
        odds3.append(thing)

print("\nOdds = " + str(odds3))
print("Evens = " + str(evens3))
