import random

# while loop (complicated)
i = 1
while i <= 2 ** 32:
    print(i)
    i *= 2

value = 0
increment = 0.5
while value < 0.999:
    value += increment
    increment *= 0.5
    print(value)

quit = "n"
while quit == "n":
    quit = input("Do you want to quit? ")

# While loop
i = 0
while i < 10:
    print(i)
    i = i + 1
# (1 + ten 1s) ten times, which is (10 * 10) + 10 = 110
a = 0
for i in range(10):
    a = a + 1
    for j in range(10):
        a = a + 1
print(a)

# How to keep a running total
total = 0
for i in range(5):
    new_number = int(input("Enter a number: "))
    total = total + new_number
print("The total is: ", total)

# How to find sum - of numbers from 1 to 100
total = 0
for i in range(1, 101):
    total = total + i
print(total)


for z in range(3):
    print("a")
    for j in range(3):
        print("b")

print("Done")

for x in range(10):
    random_chance = random.randrange(100)

    if random_chance < 3:
        print("hit")
    else:
        print("miss")


for i in range(2, 12, 2):
    print(i)

for i in range(10):
    print(i)

for i in range(1, 10, -1):
    print(i)

for i in range(3):
    print("a")

    for n in range(3):
        print("b")

print("Done")

total = 0

for i in range(5):
    user_input = int(input("Enter a number: "))
    total += user_input

print("The total is", total)

total = 0

for e in range(1, 101):
    total += e

print(total)

for i in range(5):
    print(i)

l = 0

while l < 10:
    print(l)

quit = "l"

