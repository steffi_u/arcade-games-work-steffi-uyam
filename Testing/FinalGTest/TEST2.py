"""
Steffi Uyam
Mr. Birrell
Computer Science 20
25 June 2018
"""

import random
import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
PURPLE = (163, 73, 164)

class Log(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("log.png").convert()
        self.image.set_colorkey(WHITE)
        self.rect = self.image.get_rect()


    def update(self):
        speed = 1
        self.rect.x += speed


pygame.init()

# Set the width and height of the screen [width, height]
SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720
size = (SCREEN_WIDTH, SCREEN_HEIGHT)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Bunny Crossing")

# Loop until the user clicks the close button
done = False

x_speed = 0
y_speed = 0

x_coord = SCREEN_WIDTH / 2
y_coord = SCREEN_HEIGHT / 2

x2_coord = -200
y2_coord = 120
wood_speed = 1


# Used to manage how fast the screen updates
clock = pygame.time.Clock()

pygame.mouse.set_visible(False)

background_image = pygame.image.load("river.jpg").convert()

player_image = pygame.image.load("bunny.png").convert()

player_image.set_colorkey(WHITE)

all_sprites_list = pygame.sprite.Group()
log_list = pygame.sprite.Group()


for i in range(9):
    log = Log()

    log.rect.x = 0
    log.rect.y = random.randrange(100, SCREEN_HEIGHT - 200)
    log_list.add(log)
    all_sprites_list.add(log)

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                x_speed = -5
            elif event.key == pygame.K_RIGHT:
                x_speed = 5
            elif event.key == pygame.K_UP:
                y_speed = -5
            elif event.key == pygame.K_DOWN:
                y_speed = 5

        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                x_speed = 0
            elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                y_speed = 0

        if y_coord > SCREEN_HEIGHT:
            y_coord = SCREEN_HEIGHT - 140
            y_speed = 0

    # --- Game logic should go here

    player_position = pygame.mouse.get_pos()

    x_coord += x_speed
    y_coord += y_speed
    x2_coord += wood_speed

    all_sprites_list.update()

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(WHITE)

    # --- Drawing code should go here

    screen.blit(background_image, [0, 0])
    screen.blit(player_image, [x_coord, y_coord])

    all_sprites_list.draw(screen)

    # updates the screen with what is drawn
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
