"""
 Pygame base template for opening a window

 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/

 Explanation video: http://youtu.be/vRB_983kUMc
"""

import pygame
import random
import math


# Constants

NUMBER_OF_FLAKES = 500
MAX_SIZE = 6
MIN_SIZE = 2
MAX_Y_SPEED = 6
MIN_Y_SPEED = 2

PARTY_MODE = True

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)


pygame.init()

# Set the width and height of the screen [width, height]
SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720
size = (SCREEN_WIDTH, SCREEN_HEIGHT)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()


def create_snow_flake(offscreen, party):
    # Creates a list holding info for a snow flake

    snow_flake_x = random.randrange(SCREEN_WIDTH)

    if offscreen:
        snow_flake_y = random.randrange(-15, 0)
    else:
        snow_flake_y = random.randrange(SCREEN_HEIGHT)

    if party:
        colour = (random.randrange(256), random.randrange(256),random.randrange(256))
    else:
        colour = WHITE

    snow_flake_size = random.randrange(MIN_SIZE, MAX_SIZE + 1)

    speed = random.randrange(MIN_Y_SPEED, MAX_Y_SPEED)

    angle = random.uniform(math.pi, math.pi * 2)

    snow_flake = [snow_flake_x, snow_flake_y, snow_flake_size, speed, angle, colour]

    return snow_flake


def create_snow_list(party):
    # Creates a bunch of snowflakes

    snow_list = []

    for i in range(NUMBER_OF_FLAKES):
        if party:
            snow_flake = create_snow_flake(False, True)
        else:
            snow_flake = create_snow_flake(False, False)
        snow_list.append(snow_flake)

    return snow_list


def draw_snow_flake(snow_flake):
    pygame.draw.circle(screen, snow_flake[5], [snow_flake[0], snow_flake[1]], snow_flake[2])


snow_list = create_snow_list(PARTY_MODE)


# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here

    for i in range(len(snow_list)):
        snow_list[i][1] += snow_list[i][3]

        snow_list[i][0] += int(snow_list[i][3] * math.cos(snow_list[i][4]))

        snow_list[i][4] += 0.05

        if snow_list[i][1] > SCREEN_HEIGHT:
            snow_list[i] = create_snow_flake(True, PARTY_MODE)

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(BLACK)

    # --- Drawing code should go here

    for i in range(len(snow_list)):
        draw_snow_flake(snow_list[i])

    font = pygame.font.SysFont('Vivaldi', 85, True, False)

    # Render the text. "True" means anti-aliased text.
    # Black is the color. The variable BLACK was defined
    # above as a list of [0, 0, 0]
    # Note: This line creates an image of the letters,
    # but does not put it on the screen yet.
    text = font.render("Hello", True, WHITE)

    # Put the image of the text on the screen at 250x250
    screen.blit(text, [90, 360])

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()