for row in range(10):
    for column in range(row + 1):
        print(column, end=" ")

    print()

print()

for row in range(10):
    for column in range(row):
        print(" ", end=" ")
    for column in range(10 - row):
        print(column, end=" ")

    print()

print()

for n in range(1, 10):
    for l in range(1, 10):
        if n * l < 10:
            print(" ", end="")

        print(n * l, end=" ")

    print()

print()

for row in range(1, 10):
    for j in range(10 - row):
        print(" ", end=" ")
    for j in range(1, row + 1):
        print(j, end=" ")
    for j in range(row - 1, 0, - 1):
        print(j, end=" ")

    print()

print()

for i in range(10):
    # Print leading spaces
    for j in range(10 - i):
        print(" ", end=" ")
    # Count up
    for j in range(1, i + 1):
        print(j, end=" ")
    # Count down
    for j in range(i - 1, 0, - 1):
        print(j, end=" ")
    # Next row
    print()

for i in range(10):
    # Print leading spaces
    for j in range(i + 2):
        print(" ", end=" ")
    # Count down
    for j in range(1, 9 - i):
        print(j, end=" ")
    # Next row
    print()

print()

for i in range(10):
    # Print leading spaces
    for j in range(10 - i):
        print(" ", end=" ")
    # Count up
    for j in range(1, i + 1):
        print(j, end=" ")
    # Count down
    for j in range(i - 1, 0, - 1):
        print(j, end=" ")
    # Next row
    print()

for i in range(10):
    # Print leading spaces
    for j in range(i + 2):
        print(" ", end=" ")
    # Count up
    for j in range(1, 9 - i):
        print(j, end=" ")
    # Count down
    for j in range(7 - i, 0, -1):
        print(j, end=" ")
    print()

print()

print("Count down, even if i is going up")
for i in range(10):
    x = 9 - i
    print("i is", i, "and 9-i is", x)

print()
