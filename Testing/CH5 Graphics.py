"""
 Pygame base template for opening a window

 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/

 Explanation video: http://youtu.be/vRB_983kUMc
"""

import pygame
import random

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
ANGRY_SALMON = (200, 50, 60)

# Rectangle Constants

RECT_WIDTH = 50
RECT_HEIGHT = 50

rect_colour = WHITE

pygame.init()

# Set the width and height of the screen [width, height]

SCREEN_HEIGHT = 720
SCREEN_WIDTH = 1280
size = (SCREEN_WIDTH, SCREEN_HEIGHT)
screen = pygame.display.set_mode(size)


pygame.display.set_caption("Game Title")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

rect_x = 50
rect_x_speed = 6

rect_y = 99
rect_y_speed = 6

def Random_Colour():
    red = random.randrange(256)
    blue = random.randrange(256)
    green = random.randrange(256)

    return red, blue, green

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here

    rect_x += rect_x_speed
    rect_y += rect_y_speed

    if rect_x > SCREEN_WIDTH - RECT_WIDTH:
        rect_x_speed *= -1
        RECT_WIDTH += 10
        if rect_x_speed > 0:
            rect_x_speed += 1
        else:
            rect_x_speed -= 1
        rect_x = SCREEN_WIDTH - RECT_WIDTH
        rect_colour = Random_Colour()

    if rect_x < 0:
        rect_x_speed *= -1
        RECT_WIDTH += 10
        if rect_x_speed > 0:
            rect_x_speed += 1
        else:
            rect_x_speed -= 1

    if rect_y + RECT_HEIGHT > SCREEN_HEIGHT:
        rect_y_speed *= -1
        RECT_HEIGHT += 10
        if rect_y_speed > 0:
            rect_y_speed += 1
        else:
            rect_y_speed -= 1
        rect_y = SCREEN_HEIGHT - RECT_HEIGHT
        rect_colour = Random_Colour()

    if rect_y < 0:
        rect_y_speed *= -1
        RECT_HEIGHT += 10
        if rect_y_speed > 0:
            rect_y_speed += 1
        else:
            rect_y_speed -= 1
        rect_colour = Random_Colour()

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(BLACK)

    # --- Drawing code should go here

    pygame.draw.rect(screen, rect_colour, (rect_x, rect_y, RECT_WIDTH, RECT_HEIGHT))

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()