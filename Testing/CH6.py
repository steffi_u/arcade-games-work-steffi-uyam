"""
testing
"""
import arcade
# open a window
arcade.open_window(1280, 720, "Hey")
arcade.set_background_color(arcade.color.LAVENDER_BLUSH)
# get ready to draw
arcade.start_render()
# drawing code will go here

# draw red cirlcle
arcade.draw_circle_filled(640, 360, 100, arcade.color.RED)

# draw yellow cylinder
arcade.draw_ellipse_filled(800, 360, 50, 100, arcade.color.YELLOW, 90)

# draw green straight line with thinkness of 20
arcade.draw_line(5, 650, 1275, 650, arcade.color.GREEN, 20)

# draw black triangle with thickness of 8
arcade.draw_triangle_outline(700, 600, 750, 680, 800, 600, arcade.color.BLACK, 8)

# draw blue text "Power"
arcade.draw_text("Power", 600, 100, arcade.color.BLUE, 15, 100)

# draw brick red parabola filled
arcade.draw_parabola_filled(190, 450, 300, 150, arcade.color.BRICK_RED, 270)

# draw purple outlined and filled arc
arcade.draw_arc_outline(498, 385, 330, 10, arcade.color.PURPLE, 30, 100, 80, 100)
arcade.draw_arc_filled(498, 385, 100, 10, arcade.color.PURPLE, 5, 120, 50)

# draw black text
arcade.draw_text("RING TOSS  MACHINE", 435, 197, arcade.color.BLACK, 36, 500, align='left', font_name=('Berlin Sans FB Demi'), bold=False,  italic= False, anchor_x='left', anchor_y='baseline')

# finish drawing
arcade.finish_render()
# keep the window running until someone closes it
arcade.run()