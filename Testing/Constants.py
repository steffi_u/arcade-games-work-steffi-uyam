# Screen variables

SCREEN_HEIGHT = 720
SCREEN_WIDTH = 1200

# player variables

player_x = 100

player_width = 40

player_height = 40

# pipe variables

number_pipes = 10

max_pipe_gap = 150
min_pipe_gap = 60

max_pipe_separation = 600
min_pipe_separation = 250

max_pipe_width = 125
min_pipe_width = 20

pipe_speed = 5

font_size = 50

SCORE = 0