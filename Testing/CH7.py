import arcade

# --- Set up the constants

# Size of the screen
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

# Size of the rectangle
def draw_fish(x, y):
    """ Draw a grey fish """

    # Draw a point at x, y for reference
    arcade.draw_point(x, y, arcade.color.RED, 0.1)

    # Draw head and body
    arcade.draw_arc_filled(x, y, 60, 60, (241, 80, 88), 0, 180, 320)
    arcade.draw_arc_filled(x, y, 60, 60, (241, 80, 88), 0, 180, 220)
    arcade.draw_circle_filled(x - 10, 36 + y, 9, arcade.color.BLACK)

    # Draw tail and fins
    arcade.draw_arc_filled(x + 75, y, 20, 18, (241, 80, 88), 0, 180, 30)
    arcade.draw_arc_filled(x + 75, y, 20, 18, (241, 80, 88), 0, 180, 140)
    arcade.draw_triangle_filled(x - 15, 58 + y, x + 50, 66 + y, x + 30, 46 + y, (241, 80, 88))
    arcade.draw_triangle_filled(x + 5, -58 + y, x + 35, -69 + y, x + 28, -54 + y, (241, 80, 88))



def on_draw(delta_time):
    """
    Use this function to draw everything to the screen.
    """

    # Start the render. This must happen before any drawing
    # commands. We do NOT need a stop render command.
    arcade.start_render()

    # Draw a rectangle.
    # For a full list of colors see:
    # http://pythonhosted.org/arcade/arcade.color.html
    arcade.draw_rectangle_filled(on_draw.center_x, on_draw.center_y,
                                 x, y,
                                 arcade.color.ALIZARIN_CRIMSON)

    # Modify rectangles position based on the delta
    # vector. (Delta means change. You can also think
    # of this as our speed and direction.)
    on_draw.center_x += on_draw.delta_x * delta_time
    on_draw.center_y += on_draw.delta_y * delta_time

    # Figure out if we hit the edge and need to reverse.
    if on_draw.center_x < RECT_WIDTH // 2 \
            or on_draw.center_x > SCREEN_WIDTH - RECT_WIDTH // 2:
        on_draw.delta_x *= -1
    if on_draw.center_y < RECT_HEIGHT // 2 \
            or on_draw.center_y > SCREEN_HEIGHT - RECT_HEIGHT // 2:
        on_draw.delta_y *= -1

# Below are function-specific variables. Before we use them
# in our function, we need to give them initial values. Then
# the values will persist between function calls.
#
# In other languages, we'd declare the variables as 'static' inside the
# function to get that same functionality.
#
# Later on, we'll use 'classes' to track position and velocity for multiple
# objects.
on_draw.center_x = 95  # Initial x position
on_draw.center_y = 404  # Initial y position
on_draw.delta_x = 800  # Initial change in x
on_draw.delta_y = 404  # Initial change in y


def main():
    # Open up our window
    arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, " Test ")
    arcade.set_background_color(arcade.color.WHITE)

    # Tell the computer to call the draw command at the specified interval.
    arcade.schedule(on_draw, 1 / 60)

    # Run the program
    arcade.run()


if __name__ == "__main__":
    main()