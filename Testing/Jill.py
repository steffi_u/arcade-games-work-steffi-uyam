import arcade


SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600


def draw_balloon():
    arcade.draw_circle_filled(605, 227, 68, (153, 217, 234))
    arcade.draw_circle_filled(709, 329, 68, (187, 104, 187))
    arcade.draw_line(715, 261, 721, 0, arcade.color.BLACK, 2)
    arcade.draw_line(605, 160, 637, 0, arcade.color.BLACK, 2)


def draw_smile(x, y):
    arcade.draw_ellipse_filled(x + 110, 580 + y, 75, 35, arcade.color.BLACK, 90)
    arcade.draw_ellipse_filled(x + 280, 580 + y, 75, 35, arcade.color.BLACK, 90)
    arcade.draw_parabola_filled(x + 250, 300 + y, x + 140, 100, arcade.color.BLACK, 180)


def draw_hbd(x, y):
    arcade.draw_text("Happy Birthday Jilliane!!!", x, y, (181, 230, 29), 55, 753, font_name=("Vivaldi"))


def draw_text(x, y):
    arcade.draw_text("I am so thankful to have you in my life and I hope we stay good friends after highschool <3 ur old", x, y, (136, 0, 21), 18, 333, font_name=("Georgia"))


def on_draw(delta_time):
    """ Draw everything """
    arcade.start_render()

    draw_balloon()
    draw_smile(on_draw.smile_x, -290)
    draw_hbd(20, on_draw.hbd_y)
    draw_text(240, on_draw.text_y)

# on_draw

    on_draw.smile_x += 2.3
    on_draw.hbd_y += 1.8
    on_draw.text_y += 1.8

on_draw.smile_x = -10
on_draw.hbd_y = -50
on_draw.text_y = -150



def main():
    arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "HBD J")
    arcade.set_background_color(arcade.color.BABY_PINK)

    # Call on_draw every 60th of a second.
    arcade.schedule(on_draw, 1/60)
    arcade.run()


# Call the main function to get the program started.
main()
