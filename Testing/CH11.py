# To print a list

x = [1, 2]

x[1] = 17

print(x[1])

print()

my_list = [101, 20, 10, 50, 60, "Hello"]

for thing in my_list:
    print(thing)

print()

my_list = [101, 20, 10, 50, 60, "Hello"]

for i in range(len(my_list)):  # len = length
    print(my_list[i])

print()

my_list = [101, 20, 10, 50, 60, "Hello"]

for index, value in enumerate(my_list):  # shows the index of each in the list/array
    print(index, value)

print()

my_list = [101, 20, 10, 50, 60, "Hello"]

print(my_list)

my_list.append(42)

print(my_list)

print()

# Copy of the array to modify
my_list = [5, 76, 8, 5, 3, 3, 56, 5, 23]

# Loop from 0 up to the number of elements
# in the array:
for index in range(len(my_list)):
    # Modify the element by doubling it
    my_list[index] = my_list[index] * 2

# Print the result
print(my_list)

print()

x = "Hello World!"

print(x[-6:])

print()

my_list = [[2, 3], [4, 3], [6, 7]]

for thing in my_list:
    print(thing)

print()

plain_text = "This is a test. ABC abc"

for c in plain_text:
    x = ord(c)  # shows number code for each character
    x = x + 1
    print(x, end=" ")

print()

plain_text = "This is a test. ABC abc"

for c in plain_text:
    x = ord(c)  # shows number code for character
    x = x + 1
    c2 = chr(x)  # converts back the value of each character from number codes (not exact)
    print(c2, end="")

print()

encrypted = "Uijt!jt!b!uftu/!BCD!bcd"  # converted from number codes
print()

for c in encrypted:
    x = ord(c)  # shows number code for character
    x = x - 1
    c2 = chr(x)  # converts back the original value of each character
    print(c2, end="")

print()

my_list = []

for i in range(5):
    user_inupt = input("Enter a number: ")
    user_inupt = int(user_inupt)
    my_list.append(user_inupt)
    print(my_list)

print()
