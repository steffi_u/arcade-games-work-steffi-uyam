"""
game example
"""

import Constants
import pygame
import random

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 225)
RANDOM_COLOR = (150, 50, 100)
LIGHT_BLUE = (153, 217, 234)
YELLOW = (255, 255, 0)
PINK = (255, 170, 170)
LIGHT_PINK = (255, 138, 138)
LIGHTER_PINK = (255, 187, 187)

pygame.init()

# Set the width and height of the screen [width, height]
size = (Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("FLOOPY SQUARE")

font = pygame.font.SysFont('Calibri', Constants.font_size, True, False)

text = font.render("", True, BLACK)

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()
player_color = LIGHTER_PINK

mouse_position = pygame.mouse.get_pos()

player_y = mouse_position[1]

player = pygame.Rect(Constants.player_x, player_y, Constants.player_width, Constants.player_height)

pygame.mouse.set_visible(False)

# pipes

pipe_list = []

# set starting position of first pipe

pipe_x = Constants.SCREEN_WIDTH / 2

for i in range(Constants.number_pipes):
    gap = random.randrange(Constants.min_pipe_gap, Constants.max_pipe_gap + 1)

    # set bottom of top pipe
    top = random.randrange(50, Constants.SCREEN_HEIGHT - gap)

    separation = random.randrange(Constants.min_pipe_separation, Constants.max_pipe_separation)

    width = random.randrange(Constants.min_pipe_width, Constants.max_pipe_width + 1)

    # generate a pipe

    pipe_x += separation

    top_pipe = pygame.Rect(pipe_x, 0, width, top)

    bottom_pipe = pygame.Rect(pipe_x, top + gap, width, Constants.SCREEN_HEIGHT - top - gap)

    pipe_list.append([top_pipe, bottom_pipe])

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

            # --- Game logic should go here
    mouse_position = pygame.mouse.get_pos()
    player.y = mouse_position[1]

    for i in range(len(pipe_list)):
        pipe_list[i][0].x -= Constants.pipe_speed
        pipe_list[i][1].x -= Constants.pipe_speed

        # checks for collision with bottom pipe
        if player.colliderect(pipe_list[i][0]):
            Constants.pipe_speed = 0
            player_color = RED

        if player.colliderect(pipe_list[i][1]):
            Constants.pipe_speed = 0
            player_color = RED

        if pipe_list[i][0].x < 0 - pipe_list[i][0].width:
            # reset pipe to end of list
            gap = random.randrange(Constants.min_pipe_gap, Constants.max_pipe_gap + 1)

            # set bottom of top pipe
            top = random.randrange(50, Constants.SCREEN_HEIGHT - gap)

            separation = random.randrange(Constants.min_pipe_separation, Constants.max_pipe_separation)

            width = random.randrange(Constants.min_pipe_width, Constants.max_pipe_width + 1)

            # place pipe x coordinate after last pipe

            pipe_x = pipe_list[i - 1][0].x + separation

            # generate the pipes

            top_pipe = pygame.Rect(pipe_x, 0, width, top)
            bottom_pipe = pygame.Rect(pipe_x, top + gap, width, Constants.SCREEN_HEIGHT - top - gap)

            pipe_list[i][0] = top_pipe
            pipe_list[i][1] = bottom_pipe

            Constants.SCORE += 1

            if Constants.SCORE % 5 == 0:
                Constants.pipe_speed += 1

            # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the background image.
    screen.fill(LIGHT_BLUE)

    # --- Drawing code should go here

    for i in range(len(pipe_list)):
        pygame.draw.rect(screen, PINK, pipe_list[i][0])
        pygame.draw.rect(screen, LIGHT_PINK, pipe_list[i][1])

    pygame.draw.rect(screen, player_color, player)

    # Scoring
    font = pygame.font.SysFont('Calibri', 25, True, False)

    text = font.render("Score: " + str(Constants.SCORE), True, WHITE)

    screen.blit(text, [50, 50])

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
