# FTG calculator

# input part

user_name = input("What is your name? ")

print()

print("Hello " + user_name + ". Welcome to the FTC Calculator.")

print()

fahrenheit_degrees = input("What is the temperature in fahrenheit outside? ")

fahrenheit_degrees = float(fahrenheit_degrees)

# calculate part

fahrenheit_to_celsius = (fahrenheit_degrees - 32) * 5 / 9

# output

print(user_name + ", the FTC for the temperature outside was " + str(fahrenheit_to_celsius) + ".")

print()

